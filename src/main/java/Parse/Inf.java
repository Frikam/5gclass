package Parse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Inf {
    private List<ShortSummaryElem> short_summary = new ArrayList<>();
    private List<WeightElem> weight = new ArrayList<>();

    public List<WeightElem> getWeight() {
        return weight;
    }

    public void setWeight(List<WeightElem> weight) {
        this.weight = weight;
    }

    public List<ShortSummaryElem> getShort_summary() {
        return short_summary;
    }

    public void setShort_summary(List<ShortSummaryElem> short_summary) {
        this.short_summary = short_summary;
    }
}
