package service;


import Parse.Inf;
import Parse.ShortSummaryElem;
import Parse.WeightElem;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Service {
    private double energyBurned = 0;
    private double energyIntake = 0;
    private int steps = 0;
    private double minHeartRate = 1000;
    private double maxHeartRate = -1;
    private double avgHeartRate = 0;
    private double count = 0;

    private double prevDateDay = 0;
    private SimpleDateFormat dateFormatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat dayFormatter = new SimpleDateFormat("dd");
    private SimpleDateFormat monthFormatter = new SimpleDateFormat("MM");
    private ArrayList<Map> energyBurnedJSON = new ArrayList<>();
    private ArrayList<Map> energyIntakeJSON = new ArrayList<>();
    private ArrayList<Map> heartRateJSON = new ArrayList<>();
    private ArrayList<Map> stepsJSON = new ArrayList<>();
    private ArrayList<Map> weightJSON = new ArrayList<>();
    private ArrayList<Map> finalJSON = new ArrayList<>();

    public HashMap<String, ArrayList<Map>> getFinalJSON(Inf inf) throws IOException, ParseException {
        getJSONFromShortSummary(inf.getShort_summary());
        getJSONFromWeight(inf.getWeight());

        HashMap<String, ArrayList<Map>> map = new HashMap<>();
        map.put("energy_out", energyBurnedJSON);
        map.put("energy_in", energyIntakeJSON);
        map.put("steps", stepsJSON);
        map.put("heart_rate", heartRateJSON);
        map.put("weight", weightJSON);

        return map;
    }

    public void getJSONFromShortSummary(List<ShortSummaryElem> shortSummary) throws IOException, java.text.ParseException {
        int length = shortSummary.size();
        for (int i = 0; i < length; i++) {
            double energy_in = shortSummary.get(i).getEnergy_in();
            double energy_out = shortSummary.get(i).getEnergy_out();
            double heart_rate = shortSummary.get(i).getHeart_rate();
            String stringDate = shortSummary.get(i).getAdd_date();
            Date date = dateFormatter.parse(stringDate);
            double day = Integer.parseInt(dayFormatter.format(date));
            String month = monthFormatter.format(date);

            if ((day != prevDateDay && prevDateDay != 0) || i == length - 1) {
                Map<String, Double> energyConsumedForDay = new HashMap<>();
                energyConsumedForDay.put("x", prevDateDay);
                energyConsumedForDay.put("y", energyIntake);
                Map<Integer, Map> forMonth = new HashMap<>();
                forMonth.put(Integer.valueOf(month), energyConsumedForDay);
                energyIntakeJSON.add(forMonth);


                Map<String, Double> energyBurnedForDay = new HashMap<>();
                energyBurnedForDay.put("x", prevDateDay);
                energyBurnedForDay.put("y", energyBurned);
                forMonth = new HashMap<>();
                forMonth.put(Integer.valueOf(month),energyBurnedForDay);
                energyBurnedJSON.add(forMonth);


                Map<String, Double>  stepsForDay = new HashMap<>();
                stepsForDay.put("x", prevDateDay);
                stepsForDay.put("y", (double) steps);
                forMonth = new HashMap<>();
                forMonth.put(Integer.valueOf(month),stepsForDay);
                stepsJSON.add(forMonth);


                Map<String, Double>  heartRateForDay = new HashMap<>();
                avgHeartRate /= count;
                heartRateForDay.put("x", prevDateDay);
                heartRateForDay.put("y", avgHeartRate);
                forMonth = new HashMap<>();
                forMonth.put(Integer.valueOf(month),heartRateForDay);
                heartRateJSON.add(forMonth);

                energyIntake = 0;
                energyBurned = 0;
                avgHeartRate = 0;
                steps = 0;
                count = 0;
            }


            steps += shortSummary.get(i).getSteps();
            energyIntake += energy_in;
            energyBurned += energy_out;
            avgHeartRate += heart_rate;

            if (heart_rate != 0) {
                count++;
            }

            if (heart_rate > maxHeartRate) {
                maxHeartRate = heart_rate;
            }

            if (heart_rate < minHeartRate) {
                maxHeartRate = heart_rate;
            }


            prevDateDay = day;
        }

    }
    
    public void getJSONFromWeight(List<WeightElem> weights) throws ParseException {
        dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");

        int length = weights.size();
        prevDateDay = 0;

        for (int i = 0; i < length; i++) {
            double weight = weights.get(i).getWeight();
            String stringDate = weights.get(i).getAdd_time();
            Date date = dateFormatter.parse(stringDate);
            int day = Integer.parseInt(dayFormatter.format(date));
            String month = monthFormatter.format(date);

            if ((day != prevDateDay && prevDateDay != 0) || i == length - 1) {
                if (prevDateDay == 0) {
                    prevDateDay = day;
                }
                Map<String, Double> weightForMonth = new HashMap<>();
                weightForMonth.put("x", prevDateDay);
                weightForMonth.put("y", weight);
                HashMap<Integer, Map> forMonth = new HashMap<>();

                forMonth.put(Integer.valueOf(month), weightForMonth);
                weightJSON.add(forMonth);
            }
            prevDateDay = day;
        }

    }

}
